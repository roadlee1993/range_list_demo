require "range_list/version"

module RangeList
  class List
    # the basic idea of this algothrim is to convert all the sub ranges to be arraies
    # then push all the numbers into an array to save the data
    # we will remove the duplicated elements and split it into several sub arrays as sub ranges when printing
    attr_accessor :list

    def initialize
      @list = []
    end

    # @param item [Array] the edge of the range
    def add(item)
      self.list += (item[0]...item[1]).to_a
      self.list.uniq!
    end

    # @param item [Array] the edge of the range
    def remove(item)
      self.list -= (item[0]...item[1]).to_a
    end

    # @return the formatted range
    def print
      # the result array variable is to save the splited arraies
      result = []
      temp_arr = []
      self.list.sort!
      list.each_with_index do |val, i|
        case i
        # for the first element
        when 0
          temp_arr << val
        # for the last element
        when list.length - 1
          # if it is the neiborhood of the privious element
          if val == list[i-1] + 1
            # put it into the temp array
            temp_arr << val
            # push the temp array to the final result array
            result << temp_arr
          else
            # else we push the temp array first
            result << temp_arr
            temp_arr = []
            # then push the last element to the final result array as well
            result << [val]
          end
        # for other elements
        # it is the similar logic
        else
          if val == list[i-1] + 1
            temp_arr << val
          else
            result << temp_arr
            temp_arr = [val]
          end
        end
      end

      (result.map { |arr| formatted_range(arr)}).join(' ')
    end

    private

    # @param range_array [Array] the array converted by a range
    # @return the formatted range str
    def formatted_range(range_array)
      "[#{range_array[0]}, #{range_array[-1]+1})"
    end
  end
end
