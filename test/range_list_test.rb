require "test_helper"

class RangeListTest < Minitest::Test
  def test_add_remove_print
    rl = RangeList::List.new
    rl.add([1, 5])
    assert_equal "[1, 5)", rl.print

    rl.add([10, 20])
    assert_equal "[1, 5) [10, 20)", rl.print

    rl.add([20, 20])
    assert_equal "[1, 5) [10, 20)", rl.print

    rl.add([20, 21])
    assert_equal "[1, 5) [10, 21)", rl.print

    rl.add([2, 4])
    assert_equal "[1, 5) [10, 21)", rl.print

    rl.add([3, 8])
    assert_equal "[1, 8) [10, 21)", rl.print

    rl.remove([10, 10])
    assert_equal "[1, 8) [10, 21)", rl.print

    rl.remove([10, 11])
    assert_equal "[1, 8) [11, 21)", rl.print

    rl.remove([15, 17])
    assert_equal "[1, 8) [11, 15) [17, 21)", rl.print

    rl.remove([3, 19])
    assert_equal "[1, 3) [19, 21)", rl.print
  end
end
